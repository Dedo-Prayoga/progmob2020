package ukdw.com.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class PreferenceActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);

        final Button btnPref = (Button)findViewById(R.id.btnPref);

        final SharedPreferences pref = PreferenceActivity.this.getSharedPreferences("prefs_file", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1")) {
            btnPref.setText("Logout");
        }
        else {
            btnPref.setText("Login");
        }

        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                    btnPref.setText("Logout");
                }
                else {
                    editor.putString("isLogin", "1");
                    btnPref.setText("Login");
                }
                editor.commit();
            }
        });

    }
}