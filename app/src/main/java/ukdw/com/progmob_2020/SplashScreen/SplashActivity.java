package ukdw.com.progmob_2020.SplashScreen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ukdw.com.progmob_2020.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}